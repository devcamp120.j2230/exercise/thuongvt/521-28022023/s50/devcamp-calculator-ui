import {PropsButton,PropsButton1} from "./styleButton"

function ButtonCalculator() {
    return (
        <div>
            <div>
                <div className="row">
                    <div className="col-4">
                    </div>
                    <div className="col-4">
                        <div style={{ marginBottom: "1px", flex: "1 0 auto", display: "flex", alignItems: "center", justifyContent: "center" }}>
                            <div className="row">
                                <div className="col">
                                    <PropsButton1 bgColor="#696969" style={{minWidth: "200px",minHeight: "100px"}}></PropsButton1>
                                </div>
                            </div>
                        </div>
                        <div style={{ marginBottom: "1px", flex: "1 0 auto", display: "flex", alignItems: "center", justifyContent: "center" }}>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#1C1C1C">C</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#1C1C1C">#</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#1C1C1C">%</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#1C1C1C">/</PropsButton>
                        </div>
                        <div style={{ marginBottom: "1px", flex: "1 0 auto", display: "flex", alignItems: "center", justifyContent: "center" }}>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">7</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">8</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">9</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#1C1C1C">X</PropsButton>
                        </div>
                        <div style={{ marginBottom: "1px", flex: "1 0 auto", display: "flex", alignItems: "center", justifyContent: "center" }}>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">4</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">5</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">6</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#1C1C1C">-</PropsButton>
                        </div>
                        <div style={{ marginBottom: "1px", flex: "1 0 auto", display: "flex", alignItems: "center", justifyContent: "center" }}>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">1</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">2</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">3</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#1C1C1C">+</PropsButton>
                        </div>
                        <div style={{ marginBottom: "1px", flex: "1 0 auto", display: "flex", alignItems: "center", justifyContent: "center" }}>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">.</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE">0</PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#BEBEBE"></PropsButton>
                            <PropsButton style={{ minWidth: "50px", height: "50px", }} bgColor="#1C1C1C">=</PropsButton>
                        </div>
                    </div>
                    <div className="col-4">

                    </div>
                </div>
            </div>

        </div>
    )
}

export default ButtonCalculator