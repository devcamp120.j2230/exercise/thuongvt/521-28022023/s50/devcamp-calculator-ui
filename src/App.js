
import './App.css';
import ButtonCalculator from './component/Button';

function App() {
  return (
    <div>
      <ButtonCalculator></ButtonCalculator>
    </div>
  );
}

export default App;
